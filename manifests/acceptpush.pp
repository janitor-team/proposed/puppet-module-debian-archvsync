class archvsync::acceptpush (
  $ssh_key = undef,
){

  file { '/home/ftp/.ssh':
    ensure                  => directory,
    owner                   => 'ftp',
    group                   => 'ftp',
    mode                    => '0700',
    selinux_ignore_defaults => true,
  }->
  file { '/home/ftp/.ssh/authorized_keys':
    ensure                  => file,
    owner                   => 'ftp',
    group                   => 'ftp',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => "command=\"/usr/bin/ftpsync\",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty ${ssh_key}\n",
  }

}
