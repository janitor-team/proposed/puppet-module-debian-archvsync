# == class: archvsync
#
# base glance config.
#
# === parameters:
#
#  [*manage_apache*]
#    (Optional) Install and configure Apache.
#    Defaults to true
#
#  [*manage_pureftpd*]
#    (Optional) Install and configure pureftpd.
#    Defaults to true
#
#  [*manage_rsync*]
#    (Optional) Install and configure rsync, so that other mirror
#    can mirror us.
#    Defaults to true
#
#  [*configure_rsync*]
#    (Optional) Should this module tweak /etc/rsyncd.conf and /etc/default/rsync.
#    Defaults to true
#
#  [*package_ensure*]
#    (Optional) Ensure state for package.
#    Defaults to 'present'
#
#  [*mirrorname*]
#    (Optional) Name of the mirror.
#    Defaults to the hostname's FQDN.
#
#  [*to*]
#    (Optional) Mirror destination.
#    Defaults to '/home/ftp/debian/'
#
#  [*mailto*]
#    (Optional) Email of the ftpmaster.
#    Defaults to 'toto@example.com'
#
#  [*homedir*]
#    (Optional) Mirror FTP home.
#    Defaults to '/home/ftp'
#
#  [*hub*]
#    (Optional) HUB param.
#    Defaults to 'false'
#
#  [*rsync_host*]
#    (Optional) Source to rsync from.
#    Defaults to ftp.fr.debian.org.
#
#  [*rsync_path*]
#    (Optional) Rsync path in the rsync_host.
#    Defaults to 'debian'
#
#  [*also_sync_deb_cd*]
#    (Optional) Should we also rsync /debian-cd using regular Rsync?
#    Defaults to true
#
#  [*deb_cd_path*]
#    (Optional) Remote rsync path for the debian-cd repo
#    Defaults to debian-cd
#
#  [*also_sync_deb_security*]
#    (Optional) Should we also rsync /debian-security using regular Rsync?
#    Defaults to true
#
#  [*deb_security_use_debian_org*]
#    (Optional) Use rsync.security.debian.org
#    Defaults to false
#
#  [*deb_security_path*]
#    (Optional) Remote rsync path for the debian-cd repo
#    Defaults to debian-security
#
#  [*also_sync_deb_archive*]
#    (Optional) Should we also rsync /debian-archive using regular Rsync?
#    Defaults to true
#
#  [*deb_archive_path*]
#    (Optional) Remote rsync path for the debian-archive repo
#    Defaults to debian-archive
#
#  [*also_sync_ubuntu*]
#    (Optional) Should we also rsync /debian-ubuntu using regular Rsync?
#    Defaults to true
#
#  [*ubuntu_path*]
#    (Optional) Remote rsync path for the ubuntu repo
#    Defaults to ubuntu
#
#  [*info_maintainer*]
#    (Optional) Maintainer name and email.
#    Defaults to 'Toor Op <root@localhost>'
#
#  [*info_sponsor*]
#    (Optional) Company sponsoring the mirror.
#    Defaults to 'World Company SA <https://www.example.com>'
#
#  [*info_country*]
#    (Optional) Country where the mirror will be hosted in.
#    Defaults to 'US'
#
#  [*info_location*]
#    (Optional) Location where the mirror will be hosted in.
#    Defaults to 'Nowhere city'
#
#  [*info_throughput*]
#    (Optional) Bandwidth speed of the mirror.
#    Defaults to '10Gb'
#
#  [*arch_include*]
#    (Optional) Architectures to mirror.
#    Defaults to 'amd64 source'
#
#  [*arch_exclude*]
#    (Optional) Architectures to mirror.
#    Defaults to ''
#
#  [*logdir*]
#    (Optional) Directory where logs will be written.
#    Defaults to '/home/ftp/log'
#
#  [*setup_daily_cron*]
#    (Optional) Install a cron job to do ftpsync every day.
#    Default to true
#
#  [*accept_push*]
#    (Optional) Switch to install a /home/ftp/.ssh/authorized_keys to allow
#    another mirror to send a push signal.
#    Default to false
#
#  [*push_signal_ssh_key*]
#    (Optional) Publish ssh key to accept the push
#    Default to undef
#
#  [*enable_runmirrors*]
#    (Optional) Should this server launch runmirrors to push to other
#    downstream servers
#    Default to false
#
#  [*runmirrors_hostnames*]
#    (Optional) List of downstream hostnames to push to.
#    Default to []
#
class archvsync(
  $manage_apache        = true,
  $manage_pureftpd      = true,
  $manage_rsync         = true,
  $configure_rsync      = true,
  $package_ensure       = 'present',
  $mirrorname           = $::fqdn,
  $to                   = '/home/ftp/debian/',
  $mailto               = 'toto@example.com',
  $homedir              = '/home/ftp',
  $rsync_host           = 'ftp.fr.debian.org',
  $rsync_path           = 'debian',

  $also_sync_deb_cd       = true,
  $deb_cd_path            = 'debian-cd',
  $also_sync_deb_security = true,
  $deb_security_path      = 'debian-security',
  $also_sync_deb_archive  = true,
  $deb_security_use_debian_org = false,
  $deb_archive_path       = 'debian-archive',
  $also_sync_ubuntu       = true,
  $ubuntu_path            = 'ubuntu',

  $info_maintainer      = 'Toor Op <root@localhost>',
  $info_sponsor         = 'World Company SA <https://www.example.com>',
  $info_country         = 'US',
  $info_location        = 'Nowhere city',
  $info_throughput      = '10Gb',
  $arch_include         = 'amd64 source',
  $arch_exclude         = '',

  $logdir               = '/home/ftp/log',
  $setup_daily_cron     = true,
  $accept_push          = false,
  $push_ssh_key         = undef,
  $enable_runmirrors    = false,
  $runmirrors_hostnames = [],
) {
  include ::archvsync::deps

  $hub = $enable_runmirrors

  if $arch_include == " " {
    $arch_include_real = ""
  }else{
    $arch_include_real = $arch_include
  }

  group { 'ftp':
    ensure => 'present',
  }->
  user { 'ftp':
    ensure           => 'present',
    home             => $homedir,
    password         => '!!',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/bash',
    groups           => ['ftp'],
  }->
  file { $homedir:
    ensure                  => directory,
    owner                   => ftp,
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { $to:
    ensure                  => directory,
    owner                   => ftp,
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { $logdir:
    ensure                  => directory,
    owner                   => ftp,
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "${homedir}/.config":
    ensure                  => directory,
    owner                   => ftp,
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "${homedir}/.config/ftpsync":
    ensure                  => directory,
    owner                   => ftp,
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "${homedir}/.config/ftpsync/ftpsync.conf":
    ensure                  => file,
    owner                   => ftp,
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => template("${module_name}/ftpsync.conf.erb"),
  }->
  package { 'ftpsync':
    ensure => $package_ensure,
    tag    => ['archvsync-package'],
  }

  if $setup_daily_cron and $accept_push {
    fail("Both setup_daily_cron and accept_push cannot be selected at once")
  }

  if $setup_daily_cron {
    file { '/etc/cron.daily/ftpsync':
      ensure                  => file,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
      content                 => template("${module_name}/ftpsync.erb"),
      require                 => Package['ftpsync']
    }
  }else{
    file { '/etc/cron.daily/ftpsync':
      ensure => absent,
    }
  }

  # Configure push recieve
  if $accept_push {
    class { '::archvsync::acceptpush':
      ssh_key => $push_ssh_key,
    }
  }else{
    file { '/home/ftp/.ssh/authorized_keys':
      ensure => absent,
    }
  }

  if $also_sync_deb_cd or $also_sync_deb_security or $also_sync_deb_archive or $also_sync_ubuntu {
    if $also_sync_deb_cd {
      $rsync_deb_cd = "su ftp -c 'rsync -av ${rsync_host}::${$deb_cd_path} /home/ftp/debian-cd'"
    }else{
      $rsync_deb_cd = ''
    }

    if $also_sync_deb_security {
      if $deb_security_use_debian_org {
        $rsync_host_real = 'rsync.security.debian.org'
      }else{
        $rsync_host_real = $rsync_host
      }
      $rsync_deb_security = "su ftp -c 'rsync -av ${rsync_host_real}::${$deb_security_path} /home/ftp/debian-security'"
    }else{
      $rsync_deb_security = ''
    }

    if $also_sync_deb_archive {
      $rsync_deb_archive = "su ftp -c 'rsync -av ${rsync_host}::${deb_archive_path} /home/ftp/debian-archive'"
    }else{
      $rsync_deb_archive = ''
    }

    if $also_sync_ubuntu {
      $rsync_ubuntu = "su ftp -c 'rsync -av ${rsync_host}::${ubuntu_path} /home/ftp/ubuntu'"
    }else{
      $rsync_ubuntu = ''
    }

    $rsync_script = "#!/bin/sh
set -e

${rsync_deb_cd}
${rsync_deb_archive}
${rsync_ubuntu}
exit 0
"

    $rsync_script_security = "#!/bin/sh
set -e

${rsync_deb_security}
exit 0
"

    file { '/etc/cron.daily/archvsync-rsync':
      ensure                  => file,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
      content                 => $rsync_script,
      require                 => Package['rsync']
    }

    file { '/etc/cron.hourly/archvsync-rsync':
      ensure                  => file,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
      content                 => $rsync_script_security,
      require                 => Package['rsync']
    }
  }

  # Configure Apache
  if $manage_apache {
    class { '::archvsync::apache': }
  }

  # Configure pure-ftpd
  if $manage_pureftpd {
    class { '::archvsync::pureftpd':
      package_ensure => $package_ensure,
    }
  }

  # Configure rsync, so we accept other servers to rsync us
  if $manage_rsync {
    class { '::archvsync::rsync':
      configure_rsync => $configure_rsync,
    }
  }

  # Configure so we can push to other servers
  if $enable_runmirrors {
    class { '::archvsync::runmirrors':
      homedir              => $homedir,
      runmirrors_mailto    => $mailto,
      runmirrors_logdir    => $logdir,
      runmirrors_hostnames => $runmirrors_hostnames,
    }
  }
}
